import {Noty} from "./noty/noty";
import './noty/noty.scss'

const noty = new Noty({
  type: 'warning', // success // warning // error
  classList: 'custom-classes', // string
  text: 'Its text from options', // string
  position: 'topCenter',  // 'topLeft' // 'topCenter' // 'topRight'
  timeout: 3500 // int
});

window.n = noty;
