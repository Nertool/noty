const getTemplate = (type, text, id, classList) => {
  const clsType = type ?? Noty.defaultOptions.type;
  const desc = text ?? Noty.defaultOptions.text;
  const clsList = classList ?? Noty.defaultOptions.notyClass;
  return `<div class="noty__item ${clsList} ${clsType}" data-type="item" data-id="${id}"><p class="noty__text">${desc}</p></div>`
};

export class Noty {
  constructor(options) {
    this.$body = document.querySelector('body');
    this.$btn = this.$body.querySelector('.call-noty');
    this.option = options;

    this.#setup()
  }

  static defaultOptions = {
    type: 'success',
    classList: null,
    text: 'I am an example notification!',
    position: 'topRight',
    timeout: 2000
  };

  #setup() {
    this.clickBtnHandler = this.clickBtnHandler.bind(this);
    this.$btn.addEventListener('click', this.clickBtnHandler);
  }

  clickBtnHandler() {
    if (!this.isFullListItems()) this.#render()
  }

  isFullListItems() {
    return document.querySelectorAll('[data-type="item"]').length > 4
  }

  #render() {
    const {type, text, position, classList} = this.option;
    const randomId = Math.floor(Math.random() * 1000);

    if (!this.$wrapper) this.renderWrapper(position);

    let $noty = getTemplate(type, text, randomId, classList);
    this.$wrapper.insertAdjacentHTML('beforeend', $noty);
    $noty = this.$wrapper.querySelector('[data-id="' + randomId + '"]');

    const delay = this.option.timeout ?? Noty.defaultOptions.timeout;
    setTimeout(() => this.destroy($noty), delay)
  }

  renderWrapper(clsPosition) {
    this.$wrapper = document.createElement('div');

    const position = clsPosition ?? Noty.defaultOptions.position;
    this.$wrapper.classList.add('noty', position);
    this.$body.append(this.$wrapper);
  }

  destroy(item) {
    item.remove();
    if (!this.$wrapper.querySelectorAll('[data-type="item"]').length) {
      this.destroyWrapper()
    }
  }

  destroyWrapper() {
    this.$wrapper.remove();
    this.$wrapper = null;
  }

}
